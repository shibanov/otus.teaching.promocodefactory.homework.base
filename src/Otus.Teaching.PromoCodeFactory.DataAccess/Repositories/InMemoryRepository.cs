﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IList<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new List<T>(data);
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> CreateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));
            lock (Data)
            {
                if (Data.Any(x => x.Id == entity.Id))
                    throw new ArgumentException(
                        "An entry with the same identifier already exists.", nameof(entity));
                Data.Add(entity);
            }
            return Task.FromResult(entity);
        }

        public Task<T> UpdateAsync(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            lock (Data)
            {
                var existed = Data.FirstOrDefault(x => x.Id == entity.Id);
                if (existed == null)
                    throw new ArgumentException(
                        "An entry with this identifier does not exist.", nameof(entity));
                var index = Data.IndexOf(existed);
                Data.RemoveAt(index);
                Data.Insert(index, entity);
            }
            return Task.FromResult(entity);
        }

        public Task<bool> DeleteByIdAsync(Guid id)
        {
            lock (Data)
            {
                var existed = Data.FirstOrDefault(x => x.Id == id);
                if (existed == null)
                    throw new ArgumentException(
                        "An entry with this identifier does not exist.", nameof(id));
                return Task.FromResult(Data.Remove(existed));
            }
        }
    }
}