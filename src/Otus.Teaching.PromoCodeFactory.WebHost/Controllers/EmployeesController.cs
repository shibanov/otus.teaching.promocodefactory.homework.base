﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;
        
        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <param name="id">Идентификатор сотрудника</param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return GetEmployeeResponse(employee);
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeRequest">Информация о сотруднике</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync(
            CreateEmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest();

            var id = Guid.NewGuid();

            var roles = await _rolesRepository.GetAllAsync();

            var employee = new Employee
            {
                Id = id,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                Roles = employeeRequest.Roles
                    .Select(name => roles.FirstOrDefault(r => r.Name == name))
                    .Where(role => role != null)
                    .Select(role => new Role
                    {
                        Id = role.Id,
                        Name = role.Name,
                        Description = role.Description
                    })
                    .ToList(),
                AppliedPromocodesCount = 0
            };

            var inserted = await _employeeRepository.CreateAsync(employee);
            if (inserted == null)
                return NoContent();
            
            var response = GetEmployeeResponse(inserted);
            return CreatedAtRoute(new { response.Id }, response);
        }

        /// <summary>
        /// Обновить сотрудника
        /// </summary>
        /// <param name="employeeRequest">Информация о сотруднике</param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<EmployeeResponse>> UpdateEmployeeAsync(
            UpdateEmployeeRequest employeeRequest)
        {
            if (employeeRequest == null)
                return BadRequest();

            var roles = await _rolesRepository.GetAllAsync();

            var employee = new Employee
            {
                Id = employeeRequest.Id,
                FirstName = employeeRequest.FirstName,
                LastName = employeeRequest.LastName,
                Email = employeeRequest.Email,
                Roles = employeeRequest.Roles
                    .Select(name => roles.FirstOrDefault(r => r.Name == name))
                    .Where(role => role != null)
                    .Select(role => new Role
                    {
                        Id = role.Id,
                        Name = role.Name,
                        Description = role.Description
                    })
                    .ToList(),
                AppliedPromocodesCount = employeeRequest.AppliedPromocodesCount
            };

            var updated = await _employeeRepository.UpdateAsync(employee);
            if (updated == null)
                return NoContent();
            
            var response = GetEmployeeResponse(updated);
            return AcceptedAtRoute(new {response.Id}, response);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteEmployeeAsync(Guid id)
        {
            var deleted = await _employeeRepository.DeleteByIdAsync(id);
            if (!deleted)
                return NotFound();
            
            return NoContent();
        }

        private static EmployeeResponse GetEmployeeResponse(Employee employee)
        {
            if (employee == null)
                throw new ArgumentNullException(nameof(employee));
            
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles
                    .Select(role => new RoleItemResponse()
                    {
                        Id = role.Id,
                        Name = role.Name,
                        Description = role.Description
                    })
                    .ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}